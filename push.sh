#!/bin/bash
set -e
sudo git add .
sudo git commit
sudo GIT_SSH_COMMAND="ssh -i $HOME/.ssh/id_rsa -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" git push
