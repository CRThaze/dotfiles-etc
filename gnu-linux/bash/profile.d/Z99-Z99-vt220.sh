if (locale -a | grep -q en_US.iso88591)
then
	case $TERM in
		vt*)
			export LANG=en_US.ISO-8859-1
			export LANGUAGE=
			export LC_CTYPE="en_US.ISO-8859-1"
			export LC_NUMERIC="en_US.ISO-8859-1"
			export LC_TIME="en_US.ISO-8859-1"
			export LC_COLLATE="en_US.ISO-8859-1"
			export LC_MONETARY="en_US.ISO-8859-1"
			export LC_MESSAGES="en_US.ISO-8859-1"
			export LC_PAPER="en_US.ISO-8859-1"
			export LC_NAME="en_US.ISO-8859-1"
			export LC_ADDRESS="en_US.ISO-8859-1"
			export LC_TELEPHONE="en_US.ISO-8859-1"
			export LC_MEASUREMENT="en_US.ISO-8859-1"
			export LC_IDENTIFICATION="en_US.ISO-8859-1"
			export LC_ALL=en_US.ISO-8859-1
			;;
	esac
fi
