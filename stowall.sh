#!/usr/bin/env bash

shopt -s extglob
for x in {any,"${OSTYPE%%+([[:digit:].])}"}/*
do
	if [[ -d $x ]]
	then
		sudo stow -t /etc -d $(dirname $x) $(basename $x)
	fi
done
