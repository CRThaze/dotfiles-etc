function cl() {
  cd $1
  ls
}

function cll() {
  cd $1
  ls -l
}

function inline_exrc() {
	local file_path="$1"

	if [[ ! -f "$file_path" ]]; then
		echo "File not found: $file_path"
		return 1
	fi

	# Read the file and process it
	local processed_content=$(sed '/^$/d; /^"/d' "$file_path" | tr '\n' '|')

	# Remove the trailing |
	processed_content=$(echo "$processed_content" | sed 's/|$//')

	# Output the processed content
	echo "$processed_content"
}

function whence() {
	local print_links=false
	local command=""
	local batch=false

	# Manual option parsing
	for arg in "$@"; do
		case "$arg" in
			-p)
				print_links=true
				;;
			-h)
				tabs 2
				echo "Usage:"
				echo -e "\twhence [-q] [-p | -h] <command>"
				echo -e "\t\t-h: Print Usage"
				echo -e "\t\t-p: Print each symlink path"
				echo -e "\t\t-b: Batch mode, only print paths"
				tabs
				return
				;;
			-b)
				batch=true
				;;
			-*)
				echo "Invalid option: $arg" >&2
				return
				;;
			*)
				command="$arg"
				;;
		esac
	done

	# Ensure a command was provided
	if [[ -z "$command" ]]; then
		echo "No command provided" >&2
		return
	fi

	# Find the initial path using which
	local original_path=$(which "$command" 2> /dev/null)
	local path=$original_path
	if [[ -z "$path" ]]; then
		if (type -t "$command" >/dev/null)
		then
			type=$(type -t "$command")
			echo "$command is a $type"
			if [[ "$type" == "function" ]]
			then
				echo "Run: \`type $command\` to see its definition."
			fi
			return
		else
			echo "$command not found"
			return
		fi
	fi

	# Follow symlinks
	while [[ -L "$path" ]]; do
		if [[ "$print_links" == "true" ]]
		then
			if [[ "$batch" == "false" ]]
			then
				echo -ne "Link:\t\t"
			fi
			echo "$path -> $(readlink -f "$path")"
		fi
		path=$(readlink -f "$path")
	done

	if [[ "$batch" == "false" ]]
	then
		if [[ "$path" == "$original_path" ]]
		then
			echo -ne "Executable:\t"
		else
			echo -ne "Ultimate Exe:\t"
		fi
	fi
	echo "$path"
}
