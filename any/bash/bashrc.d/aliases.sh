alias mv="mv -iv"
alias cp="cp -riv"
alias mkdir="mkdir -vp"

alias red_october="ping -c1"
alias ro="red_october"

if [ -n "$DISPLAY" ]
then
	alias clipboard="xclip -selection c -i"
	alias cb="clipboard"
fi

if [ -f /usr/share/X11/locale/en_US.UTF-8/Compose ]
then
	alias compose_ref="less /usr/share/X11/locale/en_US.UTF-8/Compose"
fi

# Editors
alias ed="ed -p': '"
if [ -x "$(command -v nvim)" ] && ! limited_terminal
then
	vim_path="$(command -v vim)"
	vimdiff_path="$(command -v vimdiff)"
	if [ -x "$vim_path" ]
	then
		alias vvim="$vim_path"
	fi
	if [ -x "$vimdiff_path" ]
	then
		alias vvimdiff="$vimdiff_path"
	fi
	alias vim=nvim
	alias vimdiff="nvim -d"
fi
if [[ "$(whence -b vi)" == "/usr/local/bin/ex" ]]
then
	alias vi='EXINIT="$(inline_exrc $HOME/.exrc)" vi'
fi

if [ -x "$(command -v xdg-open)" ]
then
	alias open="xdg-open"
fi

# cat && less
if ! limited_terminal && [[ -x "$(command -v batcat)" || -x "$(command -v bat)" ]]
then
	if [ -x "$(command -v batcat)" ]
	then
		alias bat=batcat
	fi
	alias ccat="$(which cat)"
	alias lless="$(which less)"
	alias less=bat
	alias cat=bat
fi

# ls
if ! limited_terminal
then
	if [ -x "$(command -v lsd)" ]
	then
		alias lls="$(which ls)"
		alias ls=lsd
	else
		alias ls='ls --color=auto'
	fi
fi
alias la='ls -A'
alias ll='ls -lh'
alias lla='ls -lA'
